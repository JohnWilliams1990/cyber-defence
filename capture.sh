#!/bin/bash


# Must be run as root
if [ "$(whoami)" != 'root' ]; then 
  printf "Please run this script as root\n"
  exit
fi


host=192.168.0.20

my_ip=$(ip addr | grep -w inet | tail -1 | awk '{print $2}' | sed 's|/.*||g')
interface=$(ip addr | grep $my_ip -B 2 | head -1 | sed 's|: <.*||g' | sed 's/.*://g')

# Must be run as root
if [ -z  "$(ip addr | grep enp0s3 | grep promisc -i)" ]; then 
  ifconfig $interface promisc
fi


#tcpdump -n -c 30  | sed "s/Flags.*//g" | sed 's/.*IP//g' | grep "${host}.*>" | sed 's/://g'
#tcpdump -n -c 30  | sed "s/Flags.*//g" | sed 's/.*IP//g' | grep "${my_ip}.*>" | sed 's/://g' 

dns=dns.log
nondns=non-dns.log

rm -f dns.log
rm -f non-dns.log

# sudo tshark -l | egrep " DNS " | tee $dns &
# sudo tshark -l | egrep -v " DNS " | tee $nondns & 

sudo tshark -l | egrep " DNS " > $dns &
sudo tshark -l | egrep -v " DNS " > $nondns & 

while true ; do 
  read test;
  if [ ! -z "$test" ]; then 
  test=""

    main_ip=$(cat $nondns | awk '{print $5}' | grep -v $my_ip | sort | uniq -c | sort -nr | head  -1 | awk '{print $2}')    
    cat $nondns | awk '{print $5}' | grep -v $my_ip | sort | uniq -c | sort -nr | head  -10 
    cat non-dns.log | grep $main_ip | awk '{print $10}' | grep "[0-9]" | grep -v "\." | sort | uniq -c | sort -nr | head  -10

  fi
done 

sudo kill `ps -ef |grep tshar | awk '{print $2}'`

ifconfig $interface -promisc




#
#  {ALL TRAFFIC}      {ALL DNS TRAFFIC}
#
#  {MALICIOUS TRAFFIC} =   {TRAFFIC BETWEEN TWO HOSTS} - {ALL dns TRAFFIC} 
