#!/bin/bash


printf "\nScanning Report:\n"

my_ip=$(ip addr | grep -w inet | tail -1 | awk '{print $2}' | sed 's|/.*||g')
interface=$(ip addr | grep $my_ip -B 2 | head -1 | sed 's|: <.*||g' | sed 's/.*://g')
subnet_CIDR=$(ip addr | grep -w inet | tail -1 | awk '{print $2}' | sed 's|.*/||g')

if [ $subnet_CIDR -eq 24 ]; then 
  subnet=$(echo $my_ip | sed 's/\./ /g' | awk '{print $1"."$2"."$3".0/24"}' )
elif [ $subnet_CIDR -eq 16 ]; then 
  subnet=$(echo $my_ip | sed 's/\./ /g' | awk '{print $1"."$2".0.0/16"}' )
elif [ $subnet_CIDR -eq 8 ]; then 
  subnet=$(echo $my_ip | sed 's/\./ /g' | awk '{print $1".0.0.0/8"}' )
fi

dns=dns.log
nondns=non-dns.log

main_ip=$(cat $nondns | awk '{print $5}' | grep -v $my_ip | sort | uniq -c | sort -nr | head  -1 | awk '{print $2}')

blue=$(tput setaf 4)
normal=$(tput sgr0)
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
lime_yellow=$(tput setaf 190)
powder_blue=$(tput setaf 153)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
white=$(tput setaf 7)
bright=$(tput bold)
normal=$(tput sgr0)
blink=$(tput blink)
reverse=$(tput smso)
underline=$(tput smul)

printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n"
printf "10 highest traffic ip: \n\n"
printf "  Count IP \n"
cat $nondns | awk '{print $5}' | grep -v -e $my_ip -e Broad -e Intel | sort | uniq -c | sort -nr | head -10 

printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n"
printf "10 highest traffic ip with protocol, source and destination ports: $i\n\n"
printf "  Count  IP         Prot. Src. Dest \n"
cat $nondns | awk -F '→' '{print $2}' | grep -v -e $my_ip -e Broad -e Intel | sort | uniq -c | sort -nr | head -10 

printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n"
printf "Local machines on the subnet %s: \n\n" "$subnet"
for i in `cat $nondns |\
          awk '{print $5}' |\
          grep -v $my_ip |\
          sort | uniq -c | sort -nr | head  -10 |\
          awk '{print $2}'|\
          egrep "192.|10."|\
          grep -v -e "Broad" -e "Intel"`; do 

  if [ -z "$(grep $i $dns)" ]; then 
    #printf "\tHost not using DNS --> $i\n"
    printf "\tHost not using DNS --> ${red}${blink}${i}${normal}\n"
  fi
done

printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n"
printf "Non-Local machines: \n\n" 
for i in `cat $nondns |\
          awk '{print $5}' |\
          grep -v $my_ip |\
          sort | uniq -c | sort -nr | head  -10 |\
          awk '{print $2}'|\
          grep -v -e "^192." -e "^10." -e "Broad" -e "Intel"`; do 

  if [ -z "$(grep $i $dns)" ]; then 
    #printf "\tHost not using DNS --> $i\n"
    printf "\tHost not using DNS --> ${green}${i}${normal}\n"
  fi
done
printf "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n"


################################  sources ##################################

# https://stackoverflow.com/questions/4332478/read-the-current-text-color-in-a-xterm/4332530#4332530
